let esprima = require('esprima');
let prompt = require('prompt');
let ast = require('ast-types');
let types = require('ast-types/lib/types')();
let recast = require("recast");

prompt.delimiter = "";
prompt.message = "";
prompt.start();

const schema = {properties: {">": {type: "array", empty: false}}};

function nodeToBuilder(node) {
    let builder = types.getBuilderName(node.type);
    let properties = ast.Type.def(node.type).fieldNames
        .filter(fieldName => fieldName !== "type" && node[fieldName] !== undefined)
        .map(fieldName => node[fieldName])
        .map(property => property instanceof Object && property.hasOwnProperty("type") ? nodeToBuilder(property) :
                Array.isArray(property) ? "[" + property.map(v => nodeToBuilder(v)).join(',') + "]" : JSON.stringify(property));
    return `j.${builder}(${properties.join(',')})`;
}

function nextCommand() {
    prompt.get(schema, (err, input) => {
        if(err != null) {
            console.log(err.message);
            return;
        }

        let tree = esprima.parse(input[">"].join('\n'));
        let output = recast.prettyPrint(recast.parse(nodeToBuilder(tree)), {tabWidth: 2}).code;

        console.log();
        console.log(output);
        console.log();

        nextCommand();
    });
}

console.log("^C to parse script, ^D to exit");
nextCommand();
